﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GFT
{
    class Donut : ACShape, IShape
    {
        const String nombre = "Donut";
        private Double radius1;
        private Double radius2;

        public Donut(double X, double Y, double radius1, double radius2)
        {
            this.id = get_id();
            this.X = X;
            this.Y = Y;
            //radius1 siempre el radio mayor
            //radius2 siempre el radio menor
            if (radius1 >= radius2)
            {
                this.radius1 = radius1;
                this.radius2 = radius2;
            }
            else
            {
                this.radius1 = radius2;
                this.radius2 = radius1;
            }
        }

        public string print_info()
        {
            return string.Format("Shape {0}: {1} with centre at ({2} {3}), radius big at {4} and radius small at {5}", id, nombre, X, Y, radius1, radius2);
        }

        //TODO
        public Boolean contains_coords(Double pos_x, Double pos_y)
        {

            Circle big_one;
            Circle small_one;

            big_one = new Circle(X, Y, radius1);
            small_one = new Circle(X, Y, radius2);

            if (big_one.contains_coords( pos_x, pos_y ))
            {
                if (!small_one.contains_coords(pos_x, pos_y))
                    return true;
            }

            return false;
        }

        public double surface_area()
        {
            //areas de circulos
            double area_big = 0;
            double area_small = 0;

            area_big = Math.Abs(Math.PI * (radius1 * radius1));
            area_small = Math.Abs(Math.PI * (radius2 * radius2));

            return area_big - area_small;
        }

        public string get_surface_area()
        {
            return string.Format("The surface area of the Shape {0}: {1} is {2}", id, nombre, surface_area());
        }
    }
}
