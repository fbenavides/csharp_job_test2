﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GFT
{
    class Circle : ACShape, IShape
    {
        const String nombre = "Circle";
        private Double radius;

        public Circle( double X, double Y, double radius )
        {
            this.id = get_id();
            this.X = X;
            this.Y = Y;
            this.radius = radius;
        }

        public string print_info()
        {
            return string.Format("Shape {0}: {1} with centre at ({2} {3}) and radius {4}", id, nombre, X, Y, radius );
        }

        //TODO: Arreglar esto, con este se esta tomando una forma cuadrada para
        //las posiciones, falta sacar los puntos de las esquinas curvas
        public Boolean contains_coords(Double pos_x, Double pos_y)
        {
            if (Math.Sqrt(Math.Pow(X - pos_x,2) + Math.Pow(Y - pos_y,2)) <= radius)
                return true;
            return false;
        }

        public double surface_area()
        {
            return Math.Abs(Math.PI * (radius * radius));
        }

        public string get_surface_area()
        {
            return string.Format("The surface area of the Shape {0}: {1} is {2}", id, nombre, surface_area());
        }

    }

}
