﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GFT
{
    abstract class ACShape
    {
        private static Int16 my_id = 1;

        protected Int16 id;
        protected Double X;
        protected Double Y;

        protected Int16 get_id()
        {
            return my_id++;
        }

        public int get_my_id()
        {
            return id;
        }
    }
}
