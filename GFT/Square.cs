﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GFT
{
    class Square : ACShape, IShape
    {
        const String nombre = "Square";
        private Double length;

        public Square(double X, double Y, double length)
        {
            this.id = get_id();
            this.X = X;
            this.Y = Y;
            this.length = length;
        }

        public string print_info()
        {
            return string.Format("Shape {0}: {1} with corner at ({2} {3}) and length {4}", id, nombre, X, Y, length);
        }

        public Boolean contains_coords(Double pos_x, Double pos_y)
        {
            if (length > 0 )
            {
                if (pos_x >= X && pos_x <= X + length &&
                pos_y >= Y && pos_y <= Y + length)
                {
                    return true;
                }
            }
            else
            {
                if (pos_x <= X && pos_x >= X + length &&
                pos_y <= Y && pos_y >= Y + length)
                {
                    return true;
                }
            }
            
            return false;
        }

        public double surface_area()
        {
            return Math.Abs(length * length);
        }

        public string get_surface_area()
        {
            return string.Format("The surface area of the Shape {0}: {1} is {2}", id, nombre, surface_area());
        }
    }
}
