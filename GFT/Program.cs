﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace GFT
{
    class Program
    {
        static void Main(string[] args)
        {
            const string HELP = "help";
            const string EXIT = "exit";
            const string ALL = "all";
            const string DELETE = "delete";
            const string IMPORT = "import";

            //Variables
            ShapeContainer container = new ShapeContainer();
            string comando = "";
            string[] parametros;
            string[] separador = new string[] { " " };
            Double number;

        inicio:
            try
            {
                while (true) // Loop indefinitely
                {
                    Console.WriteLine("Enter command:"); // Prompt
                    comando = Console.ReadLine().ToLower(); // Get string from user
                    if (EXIT == comando) // Check string
                    {
                        System.Environment.Exit(1);
                    }

                    parametros = comando.Split(separador, StringSplitOptions.RemoveEmptyEntries);

                    if (parametros.Length > 0)
                    {
                        //Crear una nueva forma
                        if (!(Double.TryParse(parametros[0], out number)))
                        {
                            switch (parametros[0])
                            {
                                case ShapeContainer.CIRCULO:
                                case ShapeContainer.CUADRADO:
                                case ShapeContainer.RECTANGULO:
                                case ShapeContainer.TRIANGULO:
                                case ShapeContainer.DONA:
                                    container.create_shape(parametros);
                                    break;
                                case ALL:
                                    container.show_all_shapes();
                                    break;
                                case DELETE:
                                    if (2 != parametros.Length )
                                    {
                                        Console.WriteLine("Incorrect numbers of parameter for {0} please indicate the number of the shape to delete", DELETE );
                                    }
                                    else
                                    {
                                        container.delete_shape( Convert.ToInt32(parametros[1]));
                                    }
                                    break;
                                case IMPORT:
                                    if (2 != parametros.Length)
                                    {
                                        Console.WriteLine("Incorrect numbers of parameter for {0} please indicate the path of the file to import", IMPORT);
                                    }
                                    else
                                    {
                                        if (File.Exists(parametros[1]))
                                        {
                                            import_file(parametros[1], container);
                                        }
                                    }
                                    break;
                                case HELP:
                                default:
                                    Console.WriteLine("{5}Please specify a shape ({0},{1},{2},{3},{4}) and the corresponding information for each shape.{5}All the parameter must be doubles.{5}Please review the document 'net Programming Exercise.doc' for more info.{5}Digit 'all' to view all the currents shapes.{5}Digit 'delete' and the number of the shape to delete a shape.{5}Digit 'import' and the path of the file to import a file with shapes.{5}", ShapeContainer.CIRCULO, ShapeContainer.CUADRADO, ShapeContainer.RECTANGULO, ShapeContainer.TRIANGULO, ShapeContainer.DONA, System.Environment.NewLine);
                                    break;

                            }
                        }
                        else
                        {
                            container.get_shape_contains_points(Convert.ToDouble(parametros[0]), Convert.ToDouble(parametros[1]));
                        }

                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine( e.Message );
                Console.WriteLine("Please specify a shape ({0},{1},{2},{3},{4}) and the corresponding information for each shape. All the parameter must be doubles. Please review the document 'net Programming Exercise.doc' for more info.", ShapeContainer.CIRCULO, ShapeContainer.CUADRADO, ShapeContainer.RECTANGULO, ShapeContainer.TRIANGULO, ShapeContainer.DONA);
            }
            goto inicio;
        }


        static void import_file(string import_file, ShapeContainer container)
        {
            string[] separador = new string[] { " " };
            string line;
            string[] parametros;
            StreamReader file = new StreamReader(import_file);
            while ((line = file.ReadLine() ) != null)
            {
                //Console.WriteLine(line);

                parametros = line.ToLower().Split(separador, StringSplitOptions.RemoveEmptyEntries);
                if (parametros.Length > 1)
                    container.create_shape(parametros);
            }

            file.Close();
            Console.WriteLine("File imported.");
        }
    }
}
