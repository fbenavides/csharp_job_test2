﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GFT
{
    class Triangle : ACShape, IShape
    {
        const String nombre = "Triangle";
        protected Double X2;
        protected Double Y2;
        protected Double X3;
        protected Double Y3;

        public Triangle(double X, double Y, double X2, double Y2, double X3, double Y3)
        {
            this.id = get_id();
            this.X = X;
            this.Y = Y;
            this.X2 = X2;
            this.Y2 = Y2;
            this.X3 = X3;
            this.Y3 = Y3;
        }

        public string print_info()
        {
            return string.Format("Shape {0}: {1} with vertice one at ({2} {3}), vertice two at ({4} {5}) and verice three at ({6} {7})", id, nombre, X, Y, X2, Y2, X3, Y3);
        }

        //TODO
        public Boolean contains_coords(Double pos_x, Double pos_y)
        {
            Double orientacion_base = 0;
            
            Double orientacion_punto1 = 0;
            Double orientacion_punto2 = 0;
            Double orientacion_punto3 = 0;

            orientacion_base = (X - X3) * (Y2 - Y3) - (Y - Y3) * (X2 - X3);

            orientacion_punto1 = (X - pos_x) * (Y2 - pos_y) - (Y - pos_y) * (X2 - pos_x);
            orientacion_punto2 = (X2 - pos_x) * (Y3 - pos_y) - (Y2 - pos_y) * (X3 - pos_x);
            orientacion_punto3 = (X3 - pos_x) * (Y - pos_y) - (Y3 - pos_y) * (X - pos_x);


            //Orientacion Positiva
            if (orientacion_base > 0)
            {
                if (orientacion_punto1 >= 0 && orientacion_punto2 >= 0 && orientacion_punto3 >= 0)
                    return true;
            }
            else
            {   //Orientacion Negativa
                if (orientacion_punto1 <= 0 && orientacion_punto2 <= 0 && orientacion_punto3 <= 0)
                    return true;
            }
            return false;
        }

        public double surface_area()
        {
            double temp;
            temp = (X * Y2) + (X2 * Y3) + (X3 * Y);
            temp -= (X * Y3) + (X3 * Y2) + (X2 * Y);
            temp = temp / 2;
            return Math.Abs(temp);
        }

        public string get_surface_area()
        {
            return string.Format("The surface area of the Shape {0}: {1} is {2}", id, nombre, surface_area());
        }
    }
}
