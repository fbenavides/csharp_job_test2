﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GFT
{
    interface IShape
    {
        string print_info();

        Boolean contains_coords(Double pos_x, Double pos_y);

        double surface_area();

        string get_surface_area();

        int get_my_id();
    }
}
