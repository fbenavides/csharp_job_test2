﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GFT
{
    class ShapeContainer
    {
        //Constantes con nombres de las formas
        public const string CIRCULO = "circle";
        public const string CUADRADO = "square";
        public const string RECTANGULO = "rectangle";
        public const string TRIANGULO = "triangle";
        public const string DONA = "donut";
        

        private List<IShape> shapes = new List<IShape>();

        public void add_shape( IShape shape )
        {
            shapes.Add(shape);
        }

        public void get_shape_contains_points(Double X, Double Y)
        {
            Double total_surface_area = 0;
            Int16 found = 0;

            foreach (IShape shape in shapes)
            {
                if (shape.contains_coords( X, Y ))
                {
                    Console.WriteLine(shape.print_info());
                    Console.WriteLine(shape.get_surface_area());
                    total_surface_area += shape.surface_area();
                    found++;
                }
            }

            Console.WriteLine("Total shapes found: {0}", found);
            Console.WriteLine("Total surface area: {0}", total_surface_area);
        }

        public void show_all_shapes()
        {
            Double total_surface_area = 0;

            foreach (IShape shape in shapes)
            {
                Console.WriteLine(shape.print_info());
                Console.WriteLine(shape.get_surface_area());
                total_surface_area += shape.surface_area();
            }

            Console.WriteLine("Total shapes found: {0}", shapes.Count );
            Console.WriteLine("Total surface area: {0}", total_surface_area);

        }

        public void delete_shape( int number )
        {
            IShape shape = shapes.Find(x => x.get_my_id() == number);
            if (shape != null)
            {
                shapes.Remove(shape);
                Console.WriteLine("Shapes number {0} deleted", number );
            }
            else
                Console.WriteLine("Shapes number {0} not found.", number);
        }

        public void create_shape(string[] parametros)
        {
            switch (parametros[0])
            {
                case CIRCULO:
                    if (4 != parametros.Length)
                    {
                        Console.WriteLine("Incorrect numbers of parameter for shape: {0} expected: 3", parametros[0], 4);
                    }
                    else
                    {
                        Circle obj_circle = new Circle(Convert.ToDouble(parametros[1]), Convert.ToDouble(parametros[2]), Convert.ToDouble(parametros[3]));
                        this.add_shape(obj_circle);
                        Console.WriteLine(obj_circle.print_info());
                    }
                    break;
                case CUADRADO:
                    if (4 != parametros.Length)
                    {
                        Console.WriteLine("Incorrect numbers of parameter for shape: {0} expected: 3", parametros[0], 4);
                    }
                    else
                    {
                        Square obj_square = new Square(Convert.ToDouble(parametros[1]), Convert.ToDouble(parametros[2]), Convert.ToDouble(parametros[3]));
                        this.add_shape(obj_square);
                        Console.WriteLine(obj_square.print_info());
                    }
                    break;
                case RECTANGULO:
                    if (5 != parametros.Length)
                    {
                        Console.WriteLine("Incorrect numbers of parameter for shape: {0} expected: 4", parametros[0]);
                    }
                    else
                    {
                        Rectangle obj_rectangle = new Rectangle(Convert.ToDouble(parametros[1]), Convert.ToDouble(parametros[2]), Convert.ToDouble(parametros[3]), Convert.ToDouble(parametros[4]));
                        this.add_shape(obj_rectangle);
                        Console.WriteLine(obj_rectangle.print_info());
                    }
                    break;
                case TRIANGULO:
                    if (7 != parametros.Length)
                    {
                        Console.WriteLine("Incorrect numbers of parameter for shape: {0} expected: 6", parametros[0]);
                    }
                    else
                    {
                        Triangle obj_triangle = new Triangle(Convert.ToDouble(parametros[1]), Convert.ToDouble(parametros[2]), Convert.ToDouble(parametros[3]), Convert.ToDouble(parametros[4]), Convert.ToDouble(parametros[5]), Convert.ToDouble(parametros[6]));
                        this.add_shape(obj_triangle);
                        Console.WriteLine(obj_triangle.print_info());
                    }
                    break;
                case DONA:
                    if (5 != parametros.Length)
                    {
                        Console.WriteLine("Incorrect numbers of parameter for shape: {0} expected: 4", parametros[0]);
                    }
                    else
                    {
                        Donut obj_donut = new Donut(Convert.ToDouble(parametros[1]), Convert.ToDouble(parametros[2]), Convert.ToDouble(parametros[3]), Convert.ToDouble(parametros[4]));
                        this.add_shape(obj_donut);
                        Console.WriteLine(obj_donut.print_info());
                    }
                    break;
            }
        }

    }
}
