﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GFT
{
    class Rectangle : ACShape, IShape
    {
        const String nombre = "Rectangle";
        private Double side1;
        private Double side2;

        public Rectangle(double X, double Y, double side1, double side2)
        {
            this.id = get_id();
            this.X = X;
            this.Y = Y;
            this.side1 = side1;
            this.side2 = side2;
        }

        public string print_info()
        {
            return string.Format("Shape {0}: {1} with corner at ({2} {3}) and sides: {4} - {5}", id, nombre, X, Y, side1, side2);
        }

        //TODO
        public Boolean contains_coords(Double pos_x, Double pos_y)
        {
            if (side1 > 0 && side2 > 0)
            {
                if (pos_x >= X && pos_x <= X + side1 &&
                pos_y >= Y && pos_y <= Y + side2)
                {
                    return true;
                }
            }
            else if ( side1 > 0 && side2 < 0 )
            {
                if (pos_x >= X && pos_x <= X + side1 &&
                pos_y <= Y && pos_y >= Y + side2)
                {
                    return true;
                }
            }
            else if (side1 < 0 && side2 > 0)
            {
                if (pos_x <= X && pos_x >= X + side1 &&
                pos_y >= Y && pos_y <= Y + side2)
                {
                    return true;
                }
            }
            else
            {
                if (pos_x <= X && pos_x >= X + side1 &&
                pos_y <= Y && pos_y >= Y + side2)
                {
                    return true;
                }
            }
            return false;
        }

        public double surface_area()
        {
            return Math.Abs(side1 * side2);
        }

        public string get_surface_area()
        {
            return string.Format("The surface area of the Shape {0}: {1} is {2}", id, nombre, surface_area());
        }
        
    }
}
